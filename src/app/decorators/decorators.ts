export const Component = function (options: any): Function {
  return (controller: Function) => {
    return angular.extend(options, {controller});
  };
};
