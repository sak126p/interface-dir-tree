export class MainController {
    public catalogs;
    public tree = [];


    constructor(public $log: angular.ILogService, public toastr, public $state, public $scope) {
        'ngInject';
        $scope.$on('changingCatalogs', (event, args) => {
            this.catalogs = [];
            this.tree = [];
            this.catalogs = args.catalogs;
            this.tree = this.buildTree(this.catalogs);

        });
    }

    buildTree($elements, $parentId = '#') {
        let branch = [];

        $elements.forEach((element) => {
            if (element.parent === $parentId) {
                let $children = this.buildTree($elements, element.id);
                if ($children) {
                    element.nodes = $children;
                }
                branch.push(element);
            }
        });

        return branch;
    }

}
