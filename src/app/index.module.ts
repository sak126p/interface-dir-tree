/// <reference path="../../typings/main.d.ts" />
import './common/module/underscore.module';

// config
import {config} from './index.config';
import {routerConfig} from './index.route';
import {satellizerConfig} from './common/config/satellizer.config';
import {toastrConfig} from './common/config/toastr.config';
import {themeConfig} from './common/config/theme.config';

// run
import {runBlock} from './index.run';

// components
import {tree} from './common/components/tree/tree.component';

// services
import {APIService} from './common/services/API.service';
import {CatalogService} from './common/services/catalog.service';


// controllers
import {MainController} from './pages/main/main.controller';

// filters
import {formatDate} from './common/filters/formatDate.filter';

import {AppConfig} from './app.config';

module dirTreeInterface {
    'use strict';

    angular.module('dirTreeInterface',
        [
            'ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria',
            'ngResource', 'ui.router', 'ui.bootstrap', 'satellizer', 'toastr', 'ngMaterial',
            'ngMdIcons', 'restangular', 'md.data.table', 'LocalStorageModule',
            'underscore', 'xeditable', 'ngJsTree'
        ])
        .constant('CONFIG', AppConfig)
        .config(config)
        .config(routerConfig)
        .config(satellizerConfig)
        .config(toastrConfig)
        .config(themeConfig)
        .filter('formatDate', formatDate)
        .service('API', APIService)
        .service('CatalogService', CatalogService)
        .component('tree', tree)
        .controller('mainController', MainController)
        .run(runBlock);
}
