/** @ngInject */
export function runBlock(editableOptions, editableThemes, $log, $rootScope) {

  editableThemes['angular-material'] = {
    formTpl: '<form class="editable-wrap"></form>',
    noformTpl: '<span class="editable-wrap"></span>',
    controlsTpl: '<md-input-container class="editable-controls" ng-class="{\'md-input-invalid\': $error}"></md-input-container>',
    inputTpl: '',
    errorTpl: '<div ng-messages="{message: $error}"><div class="editable-error" ng-message="message">{{$error}}</div></div>',
    buttonsTpl: '<span class="editable-buttons"></span>',
    submitTpl: '<md-button type="submit" class="md-raised md-primary">save</md-button>',
    cancelTpl: '<md-button type="button" class="md-raised md-warn" ng-click="$form.$cancel()">cancel</md-button>'
  };

  editableOptions.theme = 'angular-material';


  var deregisterationCallback = $rootScope.$on('$stateChangeStart', function (event, toState) {
    $rootScope.showLoadingPage = true;
  });

  var deregCallback = $rootScope.$on('$stateChangeSuccess', function (event, toState) {
    $rootScope.showLoadingPage = false;
  });


  $rootScope.$on('$destroy', deregCallback);
  $rootScope.$on('$destroy', deregisterationCallback);
}
