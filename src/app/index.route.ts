/** @ngInject */
export function routerConfig($stateProvider: angular.ui.IStateProvider, $urlRouterProvider: angular.ui.IUrlRouterProvider, $locationProvider) {

  let getView = ($viewName: string, $template: boolean = false) => {
    if ($template) {
      return 'app/template/' + $viewName + '/' + $viewName + '.part.html';
    } else {
      return 'app/pages/' + $viewName + '/' + $viewName + '.page.html';
    }
  };


  $locationProvider.html5Mode({
    enabled: true,
    requireBase: true
  });

  // $urlRouterProvider.deferIntercept();

  $stateProvider
    .state('app', {
      abstract: true,
      views: {
        header: {
          templateUrl: getView('header', true)
        },
        leftSidebar: {
          templateUrl: getView('left-sidebar', true)
        },
        footer: {
          templateUrl: getView('footer', true)
        }
      }
    })
    .state('app.main', {
      url: '/',
      data: {
        title: 'Main Page'
      },
      views: {
        'main@': {
          templateUrl: getView('main'),
          controller: 'mainController as mainCt'
        }
      }
    });


  $urlRouterProvider.otherwise('/');
}
