export class APIService {
  public headers: any;

  constructor(Restangular, toastr, $window, $state, CONFIG) {
    'ngInject';
    // content negotiation
    this.headers = {
      'Content-Type': 'application/json'
    };

    return Restangular.withConfig((RestangularConfigurer) => {
      RestangularConfigurer
        .setBaseUrl(CONFIG.BASEURL + '/api')
        .setDefaultHttpFields({withCredentials: false})
        .setDefaultHeaders(this.headers)
        .setErrorInterceptor((response) => {
          if (response.status !== 200) {
            toastr.error('Error: ' + response.status, response.statusText);
          }

          if (response.status === 401) {
            $state.go('login');
          }
          // if (response.status === 422) {
          //   for (var error in response.data.errors) {
          //     toastr.error(response.data.errors[error][0]);
          //   }
          // }
        });
    });
  }
}
