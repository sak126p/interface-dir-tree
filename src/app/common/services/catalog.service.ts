export class CatalogService {
  public $catalog;


  constructor(public API) {
    'ngInject';
    this.$catalog = this.API.all('catalog');
  }

  public getCatalogs() {
    return this.$catalog.customGET('getall');
  }

  public add($options) {
    return this.$catalog.all('add').post($options);
  }

  public update($options) {
    return this.$catalog.all('update').post($options);
  }

  public destroy($options) {
    return this.$catalog.all('delete').post($options);
  }


}
