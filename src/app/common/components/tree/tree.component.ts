import {Component} from '../../../decorators/decorators';

@Component({
    bindings: {},
    templateUrl: 'app/common/components/tree/tree.component.html',
    controllerAs: 'treeCt'
})

/** @ngInject */
export class tree {

    public catalogs;
    public treeConfig;
    public selected;
    public editName = false;
    public name;

    public stackForDelete = [];

    constructor(public $log: angular.ILogService, public $auth, public toastr, public CatalogService, public $scope, public $timeout, public $rootScope) {
        'ngInject';

        this.treeConfig = {
            core: {
                multiple: false,
                animation: true,
                error: function (error) {
                    $log.error('treeCtrl: error from js tree - ' + angular.toJson(error));
                },
                check_callback: true,
                worker: true
            },
            version: 1,
            plugins: [
                'contextmenu',
                'dnd'
            ]
        };

        this.$scope.treeEventsObj = {
            'dnd_stop.vakata': (data, element) => {
                // todo:obsługa DnD
            }
        };

        this.loadCatalogs();
    }

    public uniqueId() {
        return '_' + Math.random().toString(36).substr(2, 9);
    }

    public onSelected() {
        this.selected = this.$scope.treeInstance.jstree(true).get_selected();
    }

    public reCreateTree() {
        this.$rootScope.$broadcast('changingCatalogs', {catalogs: this.catalogs});
        this.treeConfig.version++;
    }

    public add() {
        this.onSelected();
        let catalog = this.catalogs.filter(catalog => catalog.id == this.selected);
        let treeStruct;
        if (this.selected.length === 0) {
            treeStruct = {
                parent: '#',
                text: 'new node',
                state: {opened: true}
            };
        } else {
            treeStruct = {
                parent: catalog[0].id,
                text: catalog[0].text,
                state: catalog[0].state
            };
        }

        this.addCatalog(treeStruct);
    }

    public change() {
        let catalog = this.catalogs.filter(catalog => catalog.id == this.selected);
        catalog[0].text = this.name;
    }

    public edit() {
        this.onSelected();
        if (this.selected.length === 0) {
            this.toastr.info('Zaznacz element');
            return 0;
        }
        let catalog = this.catalogs.filter(catalog => catalog.id == this.selected);
        this.name = catalog[0].text;
        this.editName = true;
    }

    public save() {
        let catalog = this.catalogs.filter(catalog => catalog.id == this.selected);

        this.CatalogService.update(catalog[0]).then((response) => {
            this.$log.debug(response);
            this.toastr.success(catalog[0].text, 'Został edytowany');
        });
        this.editName = false;
        this.reCreateTree();
    }

    public duplicate() {
        this.onSelected();
        if (this.selected.length !== 0) {
            let catalog = this.catalogs.filter(catalog => catalog.id == this.selected);
            this.addCatalog(catalog[0]);
        } else {
            this.toastr.info('Zaznacz element');
        }
    }

    public deleteCatalog() {
        this.onSelected();

        if (this.selected.length === 0) {
            this.toastr.info('Zaznacz element');
            return 0;
        }


        let catalogs = this.catalogs.filter(catalog => catalog.id == this.selected);

        this.createStack(catalogs[0], () => {
            this.deleteTree(this.stackForDelete.pop());
            this.$scope.treeInstance.jstree('refresh');
        });
    }

    public deleteTree(catalog) {
        this.catalogs = this.catalogs.filter(c => c.id != catalog.id);
        this.CatalogService.destroy(catalog).then((response) => {
            if (this.stackForDelete.length > 0) {
                this.deleteTree(this.stackForDelete.pop());
            } else {
                this.reCreateTree();
            }
        });

    }

    public deleteItem(catalog) {
        this.CatalogService.destroy(catalog).then((response) => {
            this.$log.debug(response);
            this.catalogs = this.catalogs.filter(item => item.id != catalog.id);
        });
    }

    public createStack(catalog, callback = null) {
        let exist = this.stackForDelete.filter(item => item.id == catalog.id);
        if (exist.length === 0) {
            let catalogs = this.catalogs.filter(item => item.parent == catalog.id);
            if (catalogs.length > 0) {
                this.stackForDelete.push(catalog);
                catalogs.forEach((c) => {
                    this.createStack(c);
                });
            } else {
                this.stackForDelete.push(catalog);
            }
        }

        if (typeof callback === 'function') {
            callback();
        }
    }

    public addCatalog(catalog) {
        let treeStruct = {
            id: null,
            parent: catalog.parent,
            text: catalog.text,
            state: catalog.state
        };

        this.$log.debug(treeStruct);
        this.CatalogService.add(treeStruct).then((response) => {
            this.$log.debug(response);
            this.toastr.success('Skopiowano');
            treeStruct.id = response.catalog.id;
            this.catalogs.push(treeStruct);
            this.reCreateTree();
        });
    }

    public loadCatalogs() {
        this.CatalogService.getCatalogs().then((response) => {
            this.$log.debug(response);

            let catalogs = [];

            response.catalogs.forEach((data) => {
                let treeStruct = {
                    id: null,
                    parent: null,
                    text: null,
                    state: null
                };
                treeStruct.id = data.id.toString();
                if (data.parent != null) {
                    treeStruct.parent = data.parent.id.toString();
                }else {
                    treeStruct.parent = '#';
                }
                treeStruct.text = data.name;
                treeStruct.state = {opened: true};
                catalogs.push(treeStruct);
            });

            this.catalogs = catalogs;
            this.reCreateTree();
        });
    }
}
