/** @ngInject */
export function themeConfig($mdThemingProvider) {
  $mdThemingProvider.theme('default')
    .primaryPalette('blue-grey', {'default': '100'})
    .accentPalette('red', {'default': '600'})
    .backgroundPalette('grey', {'default': '100'});

}
