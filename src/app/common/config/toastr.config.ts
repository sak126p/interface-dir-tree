/** @ngInject */
export function toastrConfig(toastrConfig) {
  toastrConfig.allowHtml = true;
  toastrConfig.timeOut = 3000;
  toastrConfig.positionClass = 'toast-bottom-right';
  toastrConfig.preventDuplicates = false;
  toastrConfig.progressBar = true;
}
