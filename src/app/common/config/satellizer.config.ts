/** @ngInject */
export function satellizerConfig($authProvider, CONFIG) {
  $authProvider.httpInterceptor = false;
  $authProvider.withCredentials = true;
  $authProvider.tokenRoot = null;
  $authProvider.baseUrl = CONFIG.BASEURL;
  // $authProvider.loginUrl = '/api/login';
  // $authProvider.signupUrl = '/api/register';

}
