/** @ngInject */
export function formatDate() {
  return function (input: any, momentFn) {
    var args = Array.prototype.slice.call(arguments, 2),
      momentObj = moment(input);
    return momentObj[momentFn].apply(momentObj, args);
  };
}
