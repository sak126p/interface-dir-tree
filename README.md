# README #


Wymagania:
- nodeJS
- npm
- bower
- git

Konfiguracja:

- `cd <pathProject>`

- `git clone <url> .`

- `npm install`

- `bower install`

- `gulp serve` - gdy chcemy uruchomić lokalnie razem z browser sync

- `gulp build` - gdy chcemy zbudować projekt do folderu `dist`